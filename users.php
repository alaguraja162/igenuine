<?php
session_start();
error_reporting(0);
include('includes/config.php');
// Code user Registration
if(strlen($_SESSION['igenuinelogin'])==0)
{
    header('location:login.php');
}
$username=$_SESSION['igenuine_username'];
$user_email= $_SESSION['igenuinelogin'];
date_default_timezone_set('Asia/Kolkata');// change according timezone
$currentTime = date( 'Y-m-d');
$user_query = mysqli_query($bd,"SELECT role from admin WHERE email='$user_email'");
$user_query_fetch  = mysqli_fetch_array($user_query);
$role = $user_query_fetch['role'];

$no_of_users_query = mysqli_query($bd,"SELECT COUNT(id) as no_of_users FROM users WHERE login_status='Logged In'");
$no_of_users_fetch = mysqli_fetch_array($no_of_users_query);
$no_of_users = $no_of_users_fetch['no_of_users'];

$no_of_active_users_query = mysqli_query($bd,"SELECT COUNT(id) as no_of_users FROM users WHERE status='Active'");
$no_of_active_users_fetch = mysqli_fetch_array($no_of_active_users_query);
$no_of_active_users = $no_of_active_users_fetch['no_of_users'];
?>
<!DOCTYPE html>
<html class=" ">

<head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Users</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="AKCA Application" name="description" />
    <meta content="Mazelon Technologies Pvt Ltd" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>

    <link href="assets/css/jquery-ui.css" rel="Stylesheet">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css" crossorigin="anonymous">

    <link href="dist/jquery.bootgrid.css" rel="stylesheet" />

    <script src="assets/js/jquery.js" ></script>
    <script src="assets/js/jquery-ui.js" ></script>
    <script src="assets/js/jquery-migrate-3.0.0.js" ></script>
    <script src="dist/jquery.bootgrid.min.js"></script>

    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <!-- CORE CSS TEMPLATE - END -->

    <link href="assets/css/sweetalert.css" rel="stylesheet" type="text/css"/>

    <style>
        .bootgrid-header .actionBar{
            text-align : left;
            padding-top:10px;
        }
        .form-group {
            margin-bottom: 10px;
        }

        .ui-autocomplete {
            position:absolute;
            z-index: 9999 !important;
            cursor:pointer;
            height:100px;
            overflow-y:scroll;
            background-clip: padding-box;
            background-color: #fff;
        }

        .ui-autocomplete li{
            display: block;
            clear: both;
            width:100%;
            display: block;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }

    </style>

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class=" " style="background-color: #FFFAF0;">

<?php include('includes/topbar.php'); ?>

<!-- START CONTENT -->
<section id="main-content" class=" " style="margin-left:0px;">
    <section class="wrapper main-wrapper row" style="padding: 0px 0px 0px 0px;">

        <div class="col-xs-12">

            <section class="box ">
                <header class="panel_header" style="padding:10px 10px 0px 10px;">
                    <h2 class="title pull-left">USERS</h2>
                    <button class="btn btn-success btn-icon bottom15 right15 pull-right" id="command-add" data-row-id="0" style="min-width:10px; margin-right:15px; margin-top:15px;">
                        <i class="fa fa-plus"></i> Add Users
                    </button>
                </header>
                <h6 class="uprofile-info pull-left" style="padding: 10px 40px 10px 40px;"><b>No. of Users Logged In :</b> <?php echo $no_of_users; ?></h6>
                <h6 class="uprofile-info pull-right" style="padding: 10px 40px 10px 40px;"><b>No. of Active Users :</b> <?php echo $no_of_active_users; ?></h6>
                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive" style="padding:0px 20px 20px 20px;">
                                <table id="users_grid" class="table table-condensed table-hover table-striped" width="100%" cellspacing="0" data-toggle="bootgrid">
                                    <thead>
                                    <tr>
                                        <th data-column-id="id" data-type="numeric">S.No</th>
                                        <th data-column-id="username">User Name</th>
                                        <th data-column-id="password">Password</th>
                                        <th data-column-id="request_status">Request Status</th>
                                        <th data-column-id="login_status">Login Status</th>
                                        <th data-column-id="status">Status</th>
                                        <th data-column-id="commands" data-formatter="commands" data-sortable="false">Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</section>
<div id="add_model" class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog animated bounceInDown" style="width:80%;">
        <div class="modal-content">
            <form method="post" id="frm_add">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" value="add" name="action" id="action"/>
                        <?php
                        $max_id_query=mysqli_query($bd,"select max(id) as id from users");
                        while($max_id_fetch=mysqli_fetch_array($max_id_query))
                        {
                            ?>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <div class="form-group">
                                    <label class="form-label" for="id">S.No</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" id="id" name="id" value="<?php echo $max_id_fetch['id']+1;?>" required readonly >
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label class="form-label" for="username">User Name</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="username" name="username" required >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label class="form-label" for="password">Password</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="password" name="password" required >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <div class="form-group">
                                <label class="form-label" for="status">Status</label>
                                <div class="controls">
                                    <select class="form-control" id="status" name="status" required >
                                        <option value="">Select a Status</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success" id="btn_add" type="button" disabled="disabled">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="edit_model" class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog animated bounceInDown" style="width:80%;">
        <div class="modal-content">
            <form method="post" id="frm_edit">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" value="edit" name="action" id="action"/>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <div class="form-group">
                                <label class="form-label" for="edit_id">S.No</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="edit_id" name="edit_id" required readonly >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label class="form-label" for="edit_username">User Name</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="edit_username" name="edit_username" required >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label class="form-label" for="edit_password">Password</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="edit_password" name="edit_password" required >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <div class="form-group">
                                <label class="form-label" for="edit_status">Status</label>
                                <div class="controls">
                                    <select class="form-control" id="edit_status" name="edit_status" required >
                                        <option value="">Select a Status</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success" id="btn_edit" type="button" disabled="disabled">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php include('includes/footer.php'); ?>

</body>



<!-- General section box modal start -->

<!-- modal end -->

<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

<!-- CORE JS FRAMEWORK - START -->

<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
<script>window.jQuery||document.write('<script src="assets/js/jquery-1.11.2.min.js"><\/script>');</script>
<!-- CORE JS FRAMEWORK - END -->

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="assets/js/form-validation.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

<!-- CORE TEMPLATE JS - START -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS - END -->

<script src="assets/js/sweetalert.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        var grid = $("#users_grid").bootgrid({
            ajax: true,
            rowSelect: true,
            post: function ()
            {
                /* To accumulate custom parameter with the request object */
                return {
                    id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
                };
            },

            url: "response/users_response.php",
            formatters: {
                "commands": function(column, row)
                {
                    return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-edit\"></span></button> " +
                        "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function()
        {
            /* Executes after data is loaded and rendered */
            grid.find(".command-edit").on("click", function(e)
            {
                //alert("You pressed edit on row: " + $(this).data("row-id"));
                var ele =$(this).parent();

                //console.log(grid.data());//
                $('#edit_model').modal('show');
                if($(this).data("row-id") >0) {
                    // collect the data
                    $('#edit_id').val(ele.siblings(':first').html()); // in case we're changing the key
                    $('#edit_username').val(ele.siblings(':nth-of-type(2)').html());
                    $('#edit_password').val(ele.siblings(':nth-of-type(3)').html());
                    $('#edit_status').val(ele.siblings(':nth-of-type(6)').html());
                } else {
                    alert('Now row selected! First select row, then click edit button');
                }
            }).end().find(".command-delete").on("click", function(e)
            {
                var row_id = $(this).data("row-id");
                swal({
                        title: 'Are you sure?',
                        text: 'You want to delete!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-danger',
                        confirmButtonText: 'Yes, Delete!',
                        cancelButtonText: 'No, Cancel Delete!',
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.post('response/users_response.php', { id: row_id, action:'delete'});
                            swal({title: 'Deleted!',
                                    text: 'Successfully Deleted.',
                                    type: 'success',
                                    confirmButtonClass: 'btn-danger',
                                    confirmButtonText: 'Close!',
                                    closeOnConfirm: true},
                                function(isConfirm) {
                                    if (isConfirm) {
                                        $("#users_grid").bootgrid('reload');
                                        window.location.reload();
                                    }
                                });
                        } else {
                            swal.close();
                        }
                    });
            });
        });

        function ajaxAction(action) {
            data = $("#frm_"+action).serializeArray();
            $.ajax({
                type: "POST",
                url: "response/users_response.php",
                data: data,
                dataType: "json",
                success: function(response)
                {
                    $('#'+action+'_model').modal('hide');
                    $("#users_grid").bootgrid('reload');
                    $('#frm_add').trigger("reset");
                    $('#frm_edit').trigger("reset");
                    window.location.reload();
                }
            });
        }
        $( "#command-add" ).click(function() {
            $('#add_model').modal('show');
        });
        $( "#btn_add" ).click(function() {
            ajaxAction('add');
        });
        $( "#btn_edit" ).click(function() {
            ajaxAction('edit');
        });
    });
</script>

<script>
    $(document).ready(function() {

        $('#id,#username,#password,#status').bind('change keyup', function() {
            // Your validation
            if (frm_add()) $('#btn_add').removeAttr('disabled');
        });

        function frm_add() {

            var filled = true;
            $('#id,#username,#password,#status').each(function() {
                if ($(this).val() == '') filled = false;
                else $('#btn_add').attr('disabled', 'disabled');
            });
            return filled;
        }
        $('#edit_id,#edit_username,#edit_password,#edit_status').bind('change keyup', function() {
            // Your validation
            if (frm_edit()) $('#btn_edit').removeAttr('disabled');
        });

        function frm_edit() {

            var filled = true;
            $('#edit_id,#edit_username,#edit_password,#edit_status').each(function() {
                if ($(this).val() == '') filled = false;
                else $('#btn_edit').attr('disabled', 'disabled');
            });
            return filled;
        }
    });
</script>

</html>