<?php
//include connection file
include_once("connection.php");

$db = new dbObj();
$connString =  $db->getConnstring();

$params = $_REQUEST;

$action = isset($params['action']) != '' ? $params['action'] : '';
$empCls = new Records($connString);

switch($action) {
    case 'add':
        $empCls->insertRecords($params);
        break;
    case 'edit':
        $empCls->updateRecords($params);
        break;
    case 'delete':
        $empCls->deleteRecords($params);
        break;
    default:
        $empCls->getRecords($params);
        return;
}

class Records {
    protected $conn;
    protected $data = array();
    function __construct($connString) {
        $this->conn = $connString;
    }

    public function getRecords($params) {

        $this->data = $this->getData($params);

        echo json_encode($this->data);
    }
    function insertRecords($params) {
        $data = array();;
        $sql = "INSERT INTO `apps` (id,app_name,package_name,status) VALUES('" . $params["id"] . "', '" . ucfirst($params["app_name"]) . "', '" . $params["package_name"] . "', '" . $params["status"] . "');  ";

        echo $result = mysqli_query($this->conn, $sql) or die("error to insert Ordered data");

    }


    function getData($params) {
        $rp = isset($params['rowCount']) ? $params['rowCount'] : 10;

        if (isset($params['current'])) { $page  = $params['current']; } else { $page=1; };
        $start_from = ($page-1) * $rp;

        $sql = $sqlRec = $sqlTot = $where = '';

        if( !empty($params['searchPhrase'])) {
            $where .=" WHERE ";
            $where .=" ( id LIKE '".$params['searchPhrase']."%' ";
            $where .=" OR app_name LIKE '".$params['searchPhrase']."%' ";
            $where .=" OR status LIKE '".$params['searchPhrase']."%' )";
        }
        if( !empty($params['sort']) ) {
            $where .=" ORDER By ".key($params['sort']) .' '.current($params['sort'])." ";
        }
        // getting total number records without any search
        $sql = "SELECT * FROM apps ";
        $sqlTot .= $sql;
        $sqlRec .= $sql;

        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }
        if ($rp!=-1)
            $sqlRec .= " LIMIT ". $start_from .",".$rp;


        $qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot order data");
        $queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch order data");

        while( $row = mysqli_fetch_assoc($queryRecords) ) {
            $data[] = $row;
        }

        $json_data = array(
            "current"            => intval($params['current']),
            "rowCount"            => 10,
            "total"    => intval($qtot->num_rows),
            "rows"            => $data   // total data array
        );

        return $json_data;
    }
    function updateRecords($params) {
        $data = array();
        //print_R($_POST);die;
        $sql = "Update `apps` set app_name = '" . ucfirst($params["edit_app_name"]) . "',package_name = '" . $params["edit_package_name"] . "',status = '" . $params["edit_status"] . "' WHERE id='".$_POST["edit_id"]."'";

        echo $result = mysqli_query($this->conn, $sql) or die("error to update order data");
    }

    function deleteRecords($params) {
        $data = array();
        //print_R($_POST);die;
        $sql = "delete from `apps` WHERE id='".$params["id"]."'";
        echo $result = mysqli_query($this->conn, $sql) or die("error to delete data");
        $sql1 = "ALTER TABLE `apps` DROP `id`";
        echo $result1 = mysqli_query($this->conn, $sql1) or die("error to alter table data");
        $sql2 = "ALTER TABLE `apps` AUTO_INCREMENT = 1";
        echo $result2 = mysqli_query($this->conn, $sql2) or die("error to delete aui data");
        $sql3 = "ALTER TABLE `apps` ADD `id` int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST";
        echo $result3 = mysqli_query($this->conn, $sql3) or die("error to project alter data");

    }
}
?>