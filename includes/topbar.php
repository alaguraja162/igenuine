<!-- START TOPBAR -->
<?php 
	$role = $_SESSION['igenuinerole'];
?>
<div class='page-topbar '>
	<?php 
	if($role==1){
	?>
    <a href="index.php">
		<div class='logo-area'>

		</div>
	</a>
	<?php
	}
	?>
    <div class='quick-area'>
        <div class='pull-left'>
            <ul class="info-menu left-links list-inline list-unstyled">
                <li class="notify-toggle-wrapper">
                    <?php
                        $request_query = mysqli_query($bd,"SELECT username,count(id) as cnt FROM users WHERE request_status='Requested'");
                        $request_fetch_array = mysqli_fetch_array($request_query);
                    ?>
                    <a href="#" data-toggle="dropdown" class="toggle">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-accent"><?php echo $request_fetch_array['cnt'];?></span>
                    </a>
                    <?php if($request_fetch_array['cnt']!=0) { ?>
                    <ul class="dropdown-menu notifications animated fadeIn">
                        <li class="total">
                            <span class="small">
                                You have <strong><?php echo $request_fetch_array['cnt'];?></strong> Unblock Requests.
                            </span>
                        </li>
                        <li class="list" style="max-height: 200px;">

                            <ul class="dropdown-menu-list list-unstyled ps-scrollbar">
                                <?php
                                $request_query = mysqli_query($bd,"SELECT username FROM users WHERE request_status='Requested'");
                                while($request_fetch_array=mysqli_fetch_array($request_query)) {
                                ?>
                                <li class="unread available"> <!-- available: success, warning, info, error -->
                                    <a href="users.php">
                                        <div class="notice-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div>
                                            <span class="name">
                                                <strong><?php echo $request_fetch_array['username']; ?> has requested an unblock.</strong>
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                    <?php } ?>
                </li>
            </ul>
        </div>
        <div class='pull-right'>
            <ul class="info-menu right-links list-inline list-unstyled" style="padding: 0 90px;">
				<?php if($role==1) { ?>
				<li class="profile" style="padding-right:20px;">
                    <a href="users.php" style="color:#ffffff;">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                    </a>
                </li>
				<li class="profile" style="padding-right:20px;">
                    <a href="apps.php" style="color:#ffffff;">
                        <i class="fa fa-cubes"></i>
                        <span>Apps</span>
                    </a>
                </li>
                <?php } ?>
				<li class="profile">
                    <a href="#" data-toggle="dropdown" class="toggle">
                        <img src="data/profile/profile.jpg" alt="user-image" class="img-circle img-inline">
                        <span><?php echo $username; ?> <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu profile animated fadeIn">
                        <li class="last">
                            <a href="logout.php">
                                <i class="fa fa-lock"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>			
        </div>		
    </div>

</div>
<!-- END TOPBAR -->