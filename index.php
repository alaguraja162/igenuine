<?php
session_start();
error_reporting(0);
include('includes/config.php');
// Code user Registration
if(strlen($_SESSION['igenuinelogin'])==0)
{	
	header('location:login.php');
}
$username=$_SESSION['igenuine_username'];
$user_email= $_SESSION['igenuinelogin'];
date_default_timezone_set('Asia/Kolkata');// change according timezone
$currentTime = date( 'Y-m-d');
$user_query = mysqli_query($bd,"SELECT role from admin WHERE email='$user_email'");
$user_query_fetch  = mysqli_fetch_array($user_query);
$role = $user_query_fetch['role'];
?>
<!DOCTYPE html>
<html class=" ">

<head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="AKCA Application" name="description" />
    <meta content="Mazelon Technologies Pvt Ltd" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>

    <link href="assets/css/jquery-ui.css" rel="Stylesheet">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css" crossorigin="anonymous">

    <link href="dist/jquery.bootgrid.css" rel="stylesheet" />

    <script src="assets/js/jquery.js" ></script>
    <script src="assets/js/jquery-ui.js" ></script>
    <script src="assets/js/jquery-migrate-3.0.0.js" ></script>
    <script src="dist/jquery.bootgrid.min.js"></script>

    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <!-- CORE CSS TEMPLATE - END -->

    <link href="assets/css/sweetalert.css" rel="stylesheet" type="text/css"/>

    <style>
        .bootgrid-header .actionBar{
            text-align : left;
            padding-top:10px;
        }
        .form-group {
            margin-bottom: 10px;
        }

        .ui-autocomplete {
            position:absolute;
            z-index: 9999 !important;
            cursor:pointer;
            height:100px;
            overflow-y:scroll;
            background-clip: padding-box;
            background-color: #fff;
        }

        .ui-autocomplete li{
            display: block;
            clear: both;
            width:100%;
            display: block;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }

    </style>

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class=" " style="background-color: #FFFAF0;">

<?php include('includes/topbar.php'); ?>

<!-- START CONTENT -->
<h1 style="font-size:20px;margin-top:70px;text-align: center;"><b>HOME</b></h1>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tbody><tr>
        <td align="left" valign="top">


            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tbody><tr>
                    <td align="left" valign="top" style="height:100px"></td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="height:200px"></td>
                </tr>
                <tr>
                    <td align="center" valign="top" class="tdhead" style="font-size: 20px;"><b>WELCOME TO ADMIN PANEL</b></td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="height:300px"></td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="height:100px"></td>
                </tr>
                </tbody></table>

        </td>
    </tr>
    </tbody>
</table>

<?php include('includes/footer.php'); ?>

</body>

<!-- END CONTENT -->

<!-- General section box modal start -->

<!-- modal end -->

<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

<!-- CORE JS FRAMEWORK - START -->

<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
<script>window.jQuery||document.write('<script src="assets/js/jquery-1.11.2.min.js"><\/script>');</script>
<!-- CORE JS FRAMEWORK - END -->

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="assets/js/form-validation.js" type="text/javascript"></script>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

<!-- CORE TEMPLATE JS - START -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS - END -->

<script src="assets/js/sweetalert.min.js" type="text/javascript"></script>






</html>